---
title: 'Target Audience'
image_align: left
menu: Audience
class: 'standard text-center bg-gray'
---

## Target Audience

The school is intended for graduate students and young researchers in robotics. Participants are expected from both academia and industry.

The course delivers a comprehensive overview of the basic concepts and some of the main applications of screw-theory, and hence will be particularly attractive to doctoral students and young researchers in robotics and related fields, mechanical engineering, or applied mathematics.

As has been the case in all previous editions of Summer Screws, the advanced topics and the presentation of current progress in this very active field will also be of considerable interest to many senior researchers. The key role of the presented methods in robot design and control underpins the value of the course material to robotics experts from industry.

It is recommended that attendees have their own portable computers, preferably with Matlab and Maple. Alternative equivalent software can also be used. Some experience with (and availability of) 3D CAD software would be helpful but not required.