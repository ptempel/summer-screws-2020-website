---
title: Objective
image_align: left
class: 'standard text-center bg-gray'
---

## Objective
Applications of the theory of screws are based on a combined representation of angular and linear velocity, or similarly force and moment, as a single element of a six-dimensional vector space.

The importance of screw theory in robotics is widely recognised, in principle. In practice, it is almost nowhere taught to engineering students and few know how to use it. Yet, in a variety of areas in robotics, methods and formalisms based on the geometry and algebra of screws have been shown to be superior to other techniques and have led to significant advances. These include the development of fast and efficient dynamics algorithms, discoveries in the nature of robot compliance and mechanism singularity, and the invention of numerous parallel mechanisms.

The school instructors are the authors of many of these results. They will teach the participants to apply existing techniques and to develop new ones for their own research. The basic theoretical concepts will be introduced in a rigorous manner, but the emphasis will be on applications, with numerous examples and exercises.