---
title: Scholarships
image_align: left
class: 'standard text-center bg-gray'
---

## Scholarships

In exceptional cases, the organizers may provide limited financial support and fee reduction to some participants.

To be considered, first contact the organizers **as soon as possible**.
You will be asked to provide: a short curriculum vitae; a letter of recommendation, preferably from the current academic supervisor or a senior person at the home institution, confirming that funds are not available to fully support the travel; and a budget estimate for the total expenses of the participant to attend the summer school.