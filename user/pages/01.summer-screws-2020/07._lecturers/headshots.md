---
title: Lecturers
media_order: Capture.PNG
class: standard
lecturers:
    -
        header: 'Dimiter Zlatanov'
        text: 'Dimiter has used screw theory in the singularity and mobility analysis of mechanisms. He is the inventor of one of the first-known 4-dof parallel mechanisms and has presented courses and talks on screw-based methods in various universities.'
    -
        header: 'Xianwen Kong'
        text: 'Xianwen is the inventor of numerous parallel mechanisms and the co-author of the book Type synthesis of parallel mechanisms. His results have been based on methods from screw-system theory.'
    -
        header: 'Marco Carricato<br />(to be confirmed)'
        text: 'Marco has research interests in the theory of mechanisms and robotic systems, with focus on parallel robots, cable-driven manipulators and screw theory. In this summer school, he will contribute lectures on the geometry and classification of screw systems, with applications to homokinetic couplings and examples from the theory of chains with persistent screw systems, which he originated. '
    -
        header: 'Matteo Zoppi'
        text: 'Matteo has developed screw-theoretical techniques for the derivation and application of velocity equations for complex-chain manipulators. He is also the inventor of a number of mechanisms.'
    -
        header: 'Harvey Lipkin'
        text: 'Harvey has worked more than any one on applying screw-theoretical methods in different areas of robotics and mechanisms, such as hybrid control, compliance, vibrations, and dynamics. He has taught various aspects of screw theory and supervised graduate students in the use of such methods.'
    -
        header: 'Roy Featherstone'
        text: 'Roy is the inventor of the Articulated-Body Dynamics Algorithm, and the author of the books Robot Dynamics Algorithms and Rigid Body Dynamics Algorithms. His ground-breaking work in dynamics has relied on a screw-theoretical formalism for the formulation of the equations of motion.'
    -
        header: 'Jon Selig'
        text: 'Jon is the foremost specialist on advanced geometrical and group-theoretical methods in robotics. He is the author of the book Geometric Fundamentals of Robotics, and several book chapters on the application of Clifford algebras and Lie group theory. He edited and co-authored the collection Geometrical Foundations of Robotics.'
---

## Lecturers

The lecturers are listed in the order in which they usually teach at Summer Screws.
In cooperation with local hosts around the world, all have worked hard to support our school and to ensure its success for so many years.
The content of the lectures is based to a large degree on the past and current research and teaching work of all.

Every member of this team shares a conviction that screw-theoretical methods play an increasingly central and crucial role in modern robotics.
All know each other’s work well, and agree on the main principles and ideas which animate Summer Screws’ mission.
Thus, each of the topics can be presented by a choice of several experts in that area, while all lecturers actively participate and assist attendees during the lectures, discussions, and tutorials throughout the full duration of the workshop.
A selection of five to seven teachers are present in each edition.