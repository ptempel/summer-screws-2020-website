---
title: 'Previous Summer Schools'
media_order: 'logoSS12.jpg,logoSS13.jpg,logoSS14.jpg,logoSS15.jpg,logoSS16.jpg,logoSS09.png,logoSS10.png,logoSS17.jpg'
class: 'standard text-center bg-gray'
visible: false
---

## Previous Summer Schools

<div class="container">
<div class="columns">
<div class="column col-sm-12 col-md-6 col-3" markdown="1">
[![Summer Screws 2017](logoSS17.jpg?classes=img-responsive)](http://www.dimec.unige.it/summer_screws/SS17)
</div>
<div class="column col-sm-12 col-md-6 col-3" markdown="1">
[![Summer Screws 2016](logoSS16.jpg?classes=img-responsive)](http://www.dimec.unige.it/summer_screws/SS16)
</div>
<div class="column col-sm-12 col-md-6 col-3" markdown="1">
[![Summer Screws 2015](logoSS15.jpg?classes=img-responsive)](http://www.dimec.unige.it/summer_screws/SS15)
</div>
<div class="column col-sm-12 col-md-6 col-3" markdown="1">
[![Summer Screws 2014](logoSS14.jpg?classes=img-responsive)](http://www.dimec.unige.it/pmar/summer_screws/SS14)
</div>
<div class="column col-sm-12 col-md-6 col-3" markdown="1">
[![Summer Screws 2013](logoSS13.jpg?classes=img-responsive)](http://www.dimec.unige.it/pmar/summer_screws/SS13)
</div>
<div class="column col-sm-12 col-md-6 col-3" markdown="1">
[![Summer Screws 2012](logoSS12.jpg?classes=img-responsive)](http://www.dimec.unige.it/summer_screws/SS12)
</div>
<div class="column col-sm-12 col-md-6 col-3" markdown="1">
[![Summer Screws 2010](logoSS10.png?classes=img-responsive)](http://www.dimec.unige.it/pmar/summer_screws/SS10)
</div>
<div class="column col-sm-12 col-md-6 col-3" markdown="1">
[![Summer Screws 2009](logoSS09.png?classes=img-responsive)](http://www.dimec.unige.it/summer_screws/SS09)
</div>
</div>
</div>
