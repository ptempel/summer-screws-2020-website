---
title: Announcement
image_align: left
hero_classes: ''
hero_image: ''
---

# Announcement

Dear participant/speaker,

The safety and well-being of all summer school participants is our priority. We have been closely monitoring the developing COVID-19 pandemic (Coronavirus).
The World Health Organization has declared COVID-19 a pandemic, many governments have enacted travel bans and numerous state and local governments are prohibiting large or even moderately-sized public gatherings.

Due to the COVID-19 pandemic, it has become impossible for us to organize the Summer School on Screw-Theory Based Method in Robotics at the TU Delft on the originally scheduled dates of July 25 to August 2.
Therefore, we unfortunately have to postpone the Summer School. 

The new dates are being discussed currently and will be published on this website and communicated to the participants when they are known. 

Again, we apologize for any inconvenience this has caused and please contact screws@summerscrews2020.org with any questions you may have.

Stay safe,

**Summer Screws 2020 Organizing Committee**