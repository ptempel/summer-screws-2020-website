---
title: 'Call for Participants'
media_order: SummerScrevvs2020_Call.pdf
image_align: left
menu: Call
class: 'standard text-center'
---

## Call for Participants

Following the successful editions in Beijing (2015), Montreal (2016), and Melbourne (2017) and after a three year break, the 9th IFToMM International Summer School on Screw Theory Based Methods in Robotics (Summer Screws 2020) will be organized at Delft University of Technology in Delft, The Netherlands.

The school will teach attendees how to apply existing methods and empower them to develop new ones in their own research.
The basic theoretical notions will be introduced in a rigorous manner, with emphasis on examples, applications, and exercises.
This summer school is aimed for Ph.D. students, Post-docs, M.Sc. students and other academic staff, as well as for participants from industry.

[<i class="icon icon-download"></i> Download the Call for Participants here](SummerScrevvs2020_Call.pdf){.btn .btn-primary .btn-lg}

### Registration

The registration fee is **400,- €**

The fee covers course materials, coffee breaks, and social events.

The number of registrations is limited and are assigned first come, first served.

To register, please send an email with a short curriculum vitae to screws@summerscrews2020.org.  
After registration, an invoice will be sent for payment by bank transfer.

### Important Dates

Registration starts: **March 2, 2020**

Summer school: <strike>**July 25 &ndash; August 2, 2020**</strike> **Postponed** due to the current national and international restrictions resulting from the COVID-19 pandemic.

### Program

The summer school will comprise full-day lectures and a half-day social event during the week.

A more detailed program will be available in due time.

### Location

Summer Screws 2020 will be held at Delft University of Technology, a world leading technical university with over 23,000 students.
Beautifully located in the city of artists Johannes Vermeer and Theo Jansen, it can be reached conveniently from Amsterdam Schiphol Airport (37min. by train) and Rotterdam The Hague Airport (30 min. by bike) and even by boat (Hoek van Holland / Port of Rotterdam).
The beaches of the North Sea are about 12 km, direct tram ride, from Delft: The right place to eat typically Dutch brined herring or to take a fresh swim

### Accommodation

There are numerous hotels and other accommodation options in close vicinity to the university area.  
Participants are responsible for finding their own accommodation; for advice on suitable accommodation, you may contact the [organizers](#organizers).
It is advisable to book as soon as possible due to the touristic season.

[West Cord Hotel](https://westcordhotels.com/hotel/hotel-delft/) offers a discount for guests of TU Delft, please contact us for more information.